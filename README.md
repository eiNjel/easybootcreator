# README #

EasyBoot Creator is easy-to-use tool to create bootable USB Drives with simple and elegant design.

![boot-1.png](https://bitbucket.org/repo/p4ML4g8/images/3175080480-boot-1.png)

![boot-3.png](https://bitbucket.org/repo/p4ML4g8/images/2783022757-boot-3.png)

![boot-5.png](https://bitbucket.org/repo/p4ML4g8/images/3374893053-boot-5.png)

### Tested and Working bootable OS: ###
* Arch Linux (UEFI, Legacy)
* Ubuntu (UEFI, Legacy)
* Manjaro (UEFI, Legacy)
* Parabola (UEFI, Legacy)
* Debian 8 (Legacy)
* Windows 10 (UEFI)


### Packages Required: ###
* coreutils
* util-linux
* parted
* gksu
