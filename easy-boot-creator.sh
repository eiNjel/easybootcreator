#!/bin/bash
# author: __eiNjel__

if [[ "$(id -u)" != "0" ]]; then
	echo "This script must be run as root." 1>&2
	exit
fi

cd /opt/EBC/
./EasyBoot-Creator
