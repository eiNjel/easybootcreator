#!/usr/bin/bash
# author: eiNjel

if [[ "$(id -u)" != "0" ]]; then
	echo "This script must be run as root." 1>&2
	exit
fi

function arch_linux {
	echo "Arch Linux or his derivate distro users can use makepkg -sci to install this application."
	echo "Make sure not to use sudo with makepkg command."
}

function setup {
    echo "Checking dependencies, please wait..."
    if [[ -r /bin/gksu ]]; then
        if [[ -r /bin/tar ]]; then
            if [[ -r /bin/mkfs ]]; then
                if [[ -r /bin/dd ]]; then
                    if [[ -r /bin/parted ]]; then
                        sleep 3
                        echo "Installing, please wait..."
                        cp easy-boot-creator.sh /bin/easy-boot-creator && chmod 755 /bin/easy-boot-creator
                        cp easy-boot-creator.1 /usr/share/man/man1/easy-boot-creator.1 && chmod 755 /usr/share/man/man1/easy-boot-creator.1
                        cp ebc.desktop /usr/share/applications/ebc.desktop && chmod 755 /usr/share/applications/ebc.desktop
                        mkdir /opt/EBC/
                        cp ebc.tar.gz /opt/EBC/ebc.tar.gz
                        cd /opt/EBC/ && tar xvf ebc.tar.gz && rm ebc.tar.gz
                        chmod 755 -R /opt/EBC/
                    else
                        echo "parted or libparted package not found."
                        echo "Please install parted or libparted for your Distro before trying to install this application."
                        exit
                    fi
                else
                    echo "dd command not found."
                    echo "Please install util-linux package for your Distro before trying to install this application."
                    exit
                fi
            else
                echo "mkfs command not found."
                echo "Please install coreutils package for your Distro before trying to install this application."
                exit
            fi
        else
            echo "tar command not found."
            echo "Please install tar package for your Distro to be able to install this application."
            exit
        fi
    else
        echo "gksu command not found."
        echo "Please install gksudo package for your Distro before trying to install this application."
        exit
    fi
}

if [[ -r /etc/pacman.conf ]]; then arch_linux; else setup; fi;
